﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace MDPyramid
{
    public class NodesList : List<Node>, ICloneable
    {
        public object Clone()
        {
            var list = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject<NodesList>(list);
        }

        public int ValueSum()
        {
            return this.Sum(i => i.Value);
        }

        public string Path()
        {
            return string.Join(",", this.Select(i => i.Value));
        }
    }
}
