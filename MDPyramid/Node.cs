﻿using System.Collections.Generic;

namespace MDPyramid
{
    public class Node
    {
        public Node(int value, int x, int y)
        {
            this.Value = value;
            this.X = x;
            this.Y = y;
        }
        public List<Node> Children { get; set; }
        public int Value { get; set; }
        public int Y { get; set; }
        public int X { get; set; }
        public bool IsEven()
        {
            return Value % 2 == 0;
        }
    }
}
