﻿using System;

namespace MDPyramid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Small set");
            new Pyramid(DataInputConstants.SmallSet);

            Console.WriteLine();
            Console.WriteLine("Big set");
            new Pyramid(DataInputConstants.BigSet);

            Console.ReadLine();
        }
    }
}
