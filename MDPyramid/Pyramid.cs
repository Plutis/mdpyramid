﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MDPyramid
{
    public class Pyramid
    {
        private int _biggestValue = int.MinValue;
        private NodesList _biggestValueList;
     

        public Pyramid(string input)
        {
            FindBiggestNumber(input);
            Console.WriteLine($"Max sum: {_biggestValue}");
            Console.WriteLine($"Path: {_biggestValueList.Path()}");
        }

        private void FindBiggestNumber(string data)
        {
            var nodes = Read(data);
            GoDeeper(new NodesList(), nodes.First());
        }

        private void GoDeeper(NodesList branch, Node nextNode)
        {
            LoadingIndicator.ShowLoadingAnimation();
            if (nextNode == null)
            {
                throw new Exception("Next node is null");
            }

            branch.Add(nextNode);
            var nextNodes = nextNode.Children;
            if (nextNodes.Count > 0)
            {
                bool isNotEven = !nextNode.IsEven();
                var count = nextNodes.Count(i => i.IsEven() == isNotEven);
                if (count == 1)
                {
                    GoDeeper(branch, nextNodes.FirstOrDefault(i => i.IsEven() == isNotEven));
                }
                else if (count > 1)
                {
                    GoDeeper(branch.Clone() as NodesList, nextNodes.LastOrDefault());
                    GoDeeper(branch, nextNodes.FirstOrDefault());
                }
            }
            else
            {
                var newBranchSum = branch.ValueSum();
                if (newBranchSum > _biggestValue)
                {
                    _biggestValue = newBranchSum;
                    _biggestValueList = branch.Clone() as NodesList;
                }
            }
        }

        private List<Node> Read(string data)
        {
            List<Node> nodes = new List<Node>();
            var splitByLines = data.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            int level = 0;
            foreach (var line in splitByLines)
            {
                var splitValues = line.Trim().Split(' ');
                var x = 0;
                foreach (var value in splitValues)
                {
                    var node = new Node(int.Parse(value), x++, level);
                    nodes.Add(node);
                }
                level++;
            }

            foreach (var node in nodes)
            {
                node.Children = new List<Node>(nodes.Where(i => i.Y == node.Y + 1 && (i.X == node.X || i.X == node.X + 1)));
            }

            return nodes;
        }      
    }
}
