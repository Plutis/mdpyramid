﻿using System;

namespace MDPyramid
{
    public static class LoadingIndicator
    {
        private static int _loadingCounter;

        public static void ShowLoadingAnimation()
        {
            _loadingCounter++;
            switch (_loadingCounter % 4)
            {
                case 0: Console.Write("."); break;
                case 1: Console.Write("o"); break;
                case 2: Console.Write("O"); break;
                case 3: Console.Write("o"); break;
            }
            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
        }
    }
}
